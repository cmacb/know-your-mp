from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView
from .models import Constituency


class ConstituencyDetailView(DetailView):

    model = Constituency
    slug_url_kwarg = "slug"


constituency_detail_view = ConstituencyDetailView.as_view()


class ConstituencyListView(ListView):

    model = Constituency
    slug_field = "slug"
    slug_url_kwarg = "slug"
    paginate_by = 20
    ordering = "slug"


constituency_list_view = ConstituencyListView.as_view()


class ConstituencyUpdateView(UpdateView):

    model = Constituency
    fields = ["name"]

    def get_success_url(self):
        return reverse("constituency:detail", kwargs={"name": self.request.constituency.name})

    def get_object(self):
        return Constituency.objects.get(name=self.request.constituency.name)


constituency_update_view = ConstituencyUpdateView.as_view()


class ConstituencyRedirectView(RedirectView):

    permanent = False

    def get_redirect_url(self):
        return reverse("constituency:detail", kwargs={"name": self.request.constituency.name})


constituency_redirect_view = ConstituencyRedirectView.as_view()
