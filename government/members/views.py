from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView
from .models import Member, Lord


class MemberDetailView(DetailView):

    model = Member
    slug_url_kwarg = "slug"


member_detail_view = MemberDetailView.as_view()


class MemberListView(ListView):

    model = Member
    slug_field = "slug"
    slug_url_kwarg = "slug"
    paginate_by = 20
    ordering = "slug"


member_list_view = MemberListView.as_view()


class MemberUpdateView(UpdateView):

    model = Member
    fields = ["name"]

    def get_success_url(self):
        return reverse("members:detail", kwargs={"name": self.request.member.name})

    def get_object(self):
        return Member.objects.get(name=self.request.member.name)


member_update_view = MemberUpdateView.as_view()


class MemberRedirectView(RedirectView):

    permanent = False

    def get_redirect_url(self):
        return reverse("members:detail", kwargs={"name": self.request.member.name})


member_redirect_view = MemberRedirectView.as_view()


class LordDetailView(LoginRequiredMixin, DetailView):

    model = Lord
    slug_field = "name"
    slug_url_kwarg = "name"


lord_detail_view = LordDetailView.as_view()


class LordListView(LoginRequiredMixin, ListView):

    model = Lord
    slug_field = "name"
    slug_url_kwarg = "name"


lord_list_view = LordListView.as_view()


class LordUpdateView(LoginRequiredMixin, UpdateView):

    model = Lord
    fields = ["name"]

    def get_success_url(self):
        return reverse("lords:detail", kwargs={"name": self.request.lord.name})

    def get_object(self):
        return Lord.objects.get(name=self.request.lord.name)


lord_update_view = LordUpdateView.as_view()


class LordRedirectView(LoginRequiredMixin, RedirectView):

    permanent = False

    def get_redirect_url(self):
        return reverse("lords:detail", kwargs={"name": self.request.lord.name})


lord_redirect_view = LordRedirectView.as_view()


class CabinetView(ListView):

    model = Member
    template_name = 'members/cabinet.html'
    context_object_name = 'cabinet'
    queryset = Member.objects.filter(cabinet=True)

cabinet_view = CabinetView.as_view()
