from django.apps import AppConfig


class MembersAppConfig(AppConfig):

    name = "government.members"
    verbose_name = "Members"

    def ready(self):
        try:
            import members.signals  # noqa F401
        except ImportError:
            pass
