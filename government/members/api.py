import asyncio
import aiohttp
import collections

from aiohttp import web
from contextlib import closing

from government.core.api import load_paginated_url
from government.areas.api import load_constituency
from government.members.models import Member, Person, Party

ALL_CURRENT_MEMBERS = "http://data.parliament.uk/membersdataplatform/services/mnis/members/query/House=Commons%7CIsEligible=true/"

PAGINATED_COMMONS = "/commonsmembers.json"
PAGINATED_LORDS = "http://lda.data.parliament.uk/lordsmembers.json"
PAGINATED_COMMONS_REGISTERED_INTERESTS = "http://lda.data.parliament.uk/commonsregisteredinterests.json"
PAGINATED_LORDS_REGISTERED_INTERESTS = "http://lda.data.parliament.uk/lordsregisteredinterests.json"


def load_commons():
    load_paginated_url(PAGINATED_COMMONS, extract_commons)


def extract_commons(page_number):
    for member in page_number["items"]:
        try:
            if "party" in member:
                party, _ = Party.objects.get_or_create(name=member["party"]["_value"])
            else:
                party = "independent"

            # date_of_birth = parse_datetime([0]member.DateOfBirth.cdata).date()
            person = Person.objects.create(
                name=member["fullName"]["_value"],
                gender=member["gender"]["_value"],
                party=party,
            )

            if 'homePage' in member:
                person.website = member["homePage"]
                person.save()

            if 'twitter' in member:
                person.twitter = member["twitter"]["_value"]
                person.save()

            # start_date = parse_datetime(member.HouseStartDate.cdata)
            constituency = load_constituency(member["constituency"]["_about"])

            Member.objects.create(
                person=person,
                constituency=constituency
            )

        except Exception as e:
            print(f"poorly formatted member: {member} - Exception generated: {e}")


async def get_member(base_url, page):
    url = '{}/_page={}'.format(base_url, page)
    with closing(await aiohttp.request('GET', url)) as resp:
        if resp.status == 200:
            loaded_members = await resp.read()
            print(loaded_members)
        elif resp.status == 404:
            raise web.HTTPNotFound()
        else:
            raise aiohttp.HttpProcessingError(
                code=resp.status, message=resp.reason,
                headers=resp.headers)


async def download_one(page_number, base_url, semaphore, verbose):
    try:
        with (await semaphore):
            members = await get_member(base_url, page_number)
    except web.HTTPNotFound:
        msg = 'not found'
        status = 'not found'
    except Exception as exc:
        raise FetchError(page_number) from exc
    else:
        save_member(members)
        status = 'OK'
        msg = 'OK'

    if verbose and msg:
        print(members, msg)

    return (status, page_number)


# BEGIN FLAGS2_ASYNCIO_DOWNLOAD_MANY
async def downloader_coro(member_list, base_url, verbose, concur_req):
    counter = collections.Counter()
    semaphore = asyncio.Semaphore(concur_req)
    to_do = [download_one(member, base_url, semaphore, verbose)
             for member in sorted(member_list)]

    to_do_iter = asyncio.as_completed(to_do)
    for future in to_do_iter:
        try:
            res = await future
        except FetchError as exc:
            country_code = exc.country_code
            try:
                error_msg = exc.__cause__.args[0]
            except IndexError:
                error_msg = exc.__cause__.__class__.__name__
            if verbose and error_msg:
                msg = '*** Error for {}: {}'
                print(msg.format(country_code, error_msg))
            status = "Error!"
        else:
            status = res.status

        counter[status] += 1

    return counter


def load_members_json():
    loop = asyncio.get_event_loop()


class FetchError(Exception):
    def __init__(self, member):
        self.member = member


def save_member():
    pass
