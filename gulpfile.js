
////////////////////////////////
//Setup//
////////////////////////////////

// Plugins
var gulp = require('gulp'),
    pjson = require('./package.json'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    
    rename = require('gulp-rename'),
    del = require('del'),
    plumber = require('gulp-plumber'),
    pixrem = require('gulp-pixrem'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    spawn = require('child_process').spawn,
    runSequence = require('run-sequence'),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload;


// Relative paths function
var pathsConfig = function (appName) {
    this.app = "./" + (appName || pjson.name);
    var vendorsRoot = 'node_modules/';

    return {
	
	app: this.app,
	templates: this.app + '/templates',
	css: this.app + '/static/css',
	sass: this.app + '/static/sass',
	fonts: this.app + '/static/fonts',
	images: this.app + '/static/images',
	js: this.app + '/static/js'
    };
};

var paths = pathsConfig();

////////////////////////////////
//Tasks//
////////////////////////////////

// Styles autoprefixing and minification
function styles() {
    return gulp.src(paths.sass + '/project.scss')
	.pipe(sass({
	    includePaths: [
		
		paths.sass
	    ]
	}).on('error', sass.logError))
	.pipe(plumber()) // Checks for errors
	.pipe(autoprefixer({browsers: ['last 2 versions']})) // Adds vendor prefixes
	.pipe(pixrem())  // add fallbacks for rem units
	.pipe(gulp.dest(paths.css))
	.pipe(rename({ suffix: '.min' }))
	.pipe(cssnano()) // Minifies the result
	.pipe(gulp.dest(paths.css));
};

// Javascript minification
function scripts() {
    return gulp.src(paths.js + '/project.js')
	.pipe(plumber()) // Checks for errors
	.pipe(uglify()) // Minifies the js
	.pipe(rename({ suffix: '.min' }))
	.pipe(gulp.dest(paths.js));
};

// Image compression
function imgCompression(){
    return gulp.src(paths.images + '/*')
	.pipe(imagemin()) // Compresses PNG, JPEG, GIF and SVG images
	.pipe(gulp.dest(paths.images));
}

// Run django server
function runServer(cb) {
    var cmd = spawn('docker-compose', ['-f', 'local.yml', 'up'], {stdio: 'inherit'});
    cmd.on('close', function(code) {
	console.log('runServer exited with code ' + code);
	cb(code);
    });
}

// Browser sync server for live reload
function browserSyncActivity () {
    browserSync.init(
	[paths.css + "/*.css",
	 paths.js + "*.js",
	 paths.templates + '*.html',
	],
	{
            proxy:  "localhost:8200"
	});
}

// Watch
function watch() {
    gulp.watch(paths.sass + '/*.scss', styles);
    gulp.watch(paths.js + '/*.js', scripts).on("change", reload);
    gulp.watch(paths.images + '/*', imgCompression);
    gulp.watch(paths.templates + '/**/*.html').on("change", reload);
};

var build = gulp.series(
    gulp.parallel(styles, scripts),
    gulp.parallel(runServer, browserSyncActivity, watch)
);

gulp.task(build);

// Default task
gulp.task('default', build);
